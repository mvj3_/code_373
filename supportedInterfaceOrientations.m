//1 info.plist中Supported interface orientations中加入所有方向的支持
//2 AppDelegate中加入方法 -(NSUInteger)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window{ return UIInterfaceOrientationMaskAll;} //iOS6中为了后续支持任何方向的旋转
//3 任何你想控制旋转的界面中加入方法
// iOS6.0
-(NSUInteger)supportedInterfaceOrientations{
 
    return UIInterfaceOrientationMaskPortrait;  // 可以修改为任何方向
}
 
-(BOOL)shouldAutorotate{
 
    return YES;
}
 
// iOS5.0
-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
 
    return (toInterfaceOrientation == UIInterfaceOrientationPortrait);  // 可以修改为任何方向
}
//这样你的app就可以同时支持iOS5和iOS6系统的横竖屏切换了